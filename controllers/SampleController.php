<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\EntryForm;
use app\models\PersonalDetails;
use yii\data\Pagination;

class SampleController extends Controller {

    public function actionSayHi($name = 'Nightfox') {
        return $this->render('say_hi', ['name' => $name]);
    }

    /**
     * Show and insert name and email fields
     */
    public function actionEntry() {

        $model = new EntryForm();
        $personalDetails = new PersonalDetails();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $personalDetails->name = $model->name;
            $personalDetails->email = $model->email;
            $personalDetails->save();
            $model = new EntryForm();
        }
        $details = $this->loadPersonalDetails();
        return $this->render('entry', [
                    'model' => $model,
                    'personalDetails' => $details['personalDetails'],
                    'pagination' => $details['pagination']
        ]);
    }

    /**
     * Load data from database
     */
    private function loadPersonalDetails() {

        $query = PersonalDetails::find();
        $pagination = $this->getPagination($query);

        $personalDetails = $query->orderBy('name')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        return ['pagination' => $pagination, 'personalDetails' => $personalDetails];
    }

    /**
     * Return pagination
     */
    private function getPagination($query) {

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        return $pagination;
    }

}
