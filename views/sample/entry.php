<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'email') ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>


<h2>Personal Details</h2>
<table class="table table-striped table-hovered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($personalDetails as $person): ?>
            <tr>
                <td><?= $person->id ?></td>
                <td><?= Html::encode($person->name) ?></td>
                <td><?= Html::encode($person->email) ?></td>
                <td>
                    <a class="btn btn-primary btn-xs" href="#">Edit</a>
                    <a class="btn btn-danger btn-xs" href="#">Delete</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?=
LinkPager::widget(['pagination' => $pagination])?>